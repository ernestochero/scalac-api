# scalac-api
## Overview
### Challenge Summary
GitHub portal is centered around organizations and repositories. Each organization has
many repositories and each repository has many contributors. Your goal is to create an
endpoint that given the name of the organization will return a list of contributors sorted by the
number of contributions.

### Model Description
#### GithubResponse 
This model must be formatted in JSON and sent as a response.
```scala
final case class GithubResponse(name: String, contributions: Int)
```
#### Repository
This model has certain information about a github repository
```scala
final case class Repository(id: Long, name: String, contributors_url: String)
```

#### Contribution
This model has certain information about the user's contribution 
```scala
final case class Contribution(login: String, contributions: Int)
```
### Endpoints
1. Get a list of contributors sorted by the number of contributions from all repositories in the organization
```routes
GET   /org/{org_name}/contributors
```
example success response: 
```json
[
  {"name":"mschuwalow", "contributions":56},
  {"name":"RaasAhsan",  "contributions":51},
  {"name":"adamgfraser","contributions":40},
  {"name":"iravid",     "contributions":30},
  {"name":"vasilmkd",   "contributions":20}
  {...}
]
```
Also, you could limit the number of repositories that you want to get the information  
2. Get the list of all contributors sorted by the number of contributions from a limited number of repositories.
```routes
GET   /org/{org_name}/contributors?limit_repo=2
```
example success response (This information is from the first 2 repositories):
```json
[
  {"name":"mschuwalow", "contributions":10},
  {"name":"RaasAhsan",  "contributions":5},
  {"name":"adamgfraser","contributions":1}
]
```
Also, you could limit the number of contributors that you want to get the information per each repository.  
3. Get the list of the first 2 contributors per repository, sorted by the number of contributions.
```routes
GET   /org/{org_name}/contributors?limit_contributors=2
```
```json
[
  {"name":"mschuwalow", "contributions":10},
  {"name":"RaasAhsan",  "contributions":5},
  {"name":"adamgfraser","contributions":5}
]
```
Finally, we can combine these two parameters, to obtain the information of a limited number of contributors from a limited number of repositories.  
4. Get the maximum contributor in each of the first 5 repositories.
```routes
GET   /org/{org_name}/contributors?limit_repo=5&limit_contributors=1
```
```json
[
  {"name":"mschuwalow", "contributions":10},
  {"name":"RaasAhsan",  "contributions":5}
]
```
## instructions to run the project
1\. clone project : `git clone https://bitbucket.org/ernestochero/scalac-api.git` 

2\. go into scalac-api and execute api : `cd scalacapi` && `sbt -DGH_TOKEN=<your_github_token> run`
