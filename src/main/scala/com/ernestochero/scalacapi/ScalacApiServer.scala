package com.ernestochero.scalacapi

import cats.effect.{ ConcurrentEffect, ContextShift, Timer }
import fs2.Stream
import org.http4s.client.blaze.BlazeClientBuilder
import org.http4s.implicits.http4sKleisliResponseSyntaxOptionT
import org.http4s.server.blaze.BlazeServerBuilder
import org.http4s.server.middleware.Logger
import org.http4s.{ AuthScheme, Credentials }
import org.http4s.headers.Authorization

import scala.concurrent.ExecutionContext.global

object ScalacApiServer {
  def stream[F[_]: ConcurrentEffect](appConfig: AppConfig)(
    implicit T: Timer[F],
    C: ContextShift[F],
    H: HttpErrorHandler[F, Throwable]
  ): Stream[F, Nothing] = {
    for {
      client <- BlazeClientBuilder[F](global).stream
      bearerToken  = Authorization(Credentials.Token(AuthScheme.Bearer, appConfig.token))
      githubImpl   = GithubRepositoryImpl.implementation[F](client, bearerToken)
      httpApp      = ScalacApiRoutes.githubRoutes[F](githubImpl).orNotFound
      finalHttpApp = Logger.httpApp(logHeaders = true, logBody = true)(httpApp)
      exitCode <- BlazeServerBuilder[F](global)
        .bindHttp(appConfig.httpConf.port, appConfig.httpConf.host)
        .withHttpApp(finalHttpApp)
        .serve
    } yield exitCode
  }.drain
}
