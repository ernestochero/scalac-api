package com.ernestochero.scalacapi

import cats.effect.{ Blocker, ExitCode, IO, IOApp }
import cats.implicits._

object Main extends IOApp {
  def run(args: List[String]): IO[ExitCode] = {
    implicit val githubHttpErrorHandler: HttpErrorHandler[IO, Throwable] =
      new GithubRepositoryErrorHandler[IO]
    for {
      appConfig <- Blocker[IO].use(ConfigLoader.readConfig[IO])
      exitCode  <- ScalacApiServer.stream[IO](appConfig).compile.drain.as(ExitCode.Success)
    } yield exitCode
  }
}
