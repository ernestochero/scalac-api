package com.ernestochero.scalacapi

import cats.{ ApplicativeError, MonadError }
import cats.data.{ Kleisli, OptionT }
import com.ernestochero.scalacapi.GithubRepository.GithubError
import org.http4s.{ HttpRoutes, Request, Response }
import org.http4s.dsl.Http4sDsl
import cats.implicits._

trait HttpErrorHandler[F[_], E <: Throwable] {
  def handle(routes: HttpRoutes[F]): HttpRoutes[F]
}

object HttpErrorHandler {
  def apply[F[_], E <: Throwable](implicit ev: HttpErrorHandler[F, E]) = ev
}

object RoutesHttpErrorHandler {
  def apply[F[_], E <: Throwable](
    routes: HttpRoutes[F]
  )(handler: E => F[Response[F]])(implicit ev: ApplicativeError[F, E]): HttpRoutes[F] =
    Kleisli { req: Request[F] =>
      OptionT {
        routes.run(req).value.handleErrorWith(e => handler(e).map(Option(_)))
      }
    }
}

class GithubRepositoryErrorHandler[F[_]](implicit M: MonadError[F, Throwable])
    extends HttpErrorHandler[F, Throwable] {
  val dsl = new Http4sDsl[F] {}
  import dsl._
  private val handler: Throwable => F[Response[F]] = {
    case GithubError(message) => NotFound(message)
    case ex: Throwable        => BadRequest(ex.getMessage)
  }
  override def handle(routes: HttpRoutes[F]): HttpRoutes[F] =
    RoutesHttpErrorHandler(routes)(handler)
}
