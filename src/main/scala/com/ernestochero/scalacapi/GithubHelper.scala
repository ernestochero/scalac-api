package com.ernestochero.scalacapi

import com.ernestochero.scalacapi.Constants.PerPageTag
import com.ernestochero.scalacapi.GithubRepository.{ Contribution, GithubResponse }

object GithubHelper {
  def processContributors(contributors: List[Contribution]): List[GithubResponse] =
    contributors
      .groupBy(_.login)
      .transform((name, contr) => GithubResponse(name, contr.map(_.contributions).sum))
      .values
      .toList
      .sortBy(_.contributions)(Ordering[Long].reverse)

  def buildPaginationParam(maybeValue: Option[Int]): Map[String, Int] = {
    val emptyMap = Map.empty[String, Int]
    maybeValue.fold(emptyMap)(limit => Map(PerPageTag -> limit))
  }
}
