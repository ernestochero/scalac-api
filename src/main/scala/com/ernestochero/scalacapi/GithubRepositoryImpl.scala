package com.ernestochero.scalacapi

import cats.effect.Sync
import com.ernestochero.scalacapi.GithubRepository.{
  Contribution,
  GithubError,
  GithubResponse,
  Repository
}
import org.http4s.Method.GET
import org.http4s.{ EntityDecoder, Uri }
import org.http4s.client.Client
import org.http4s.client.dsl.Http4sClientDsl
import org.http4s.headers.Authorization
import cats.implicits._
import com.ernestochero.scalacapi.Constants.{ GithubBaseUrl, OrgTag, RepoTag }
import com.ernestochero.scalacapi.GithubHelper.{ buildPaginationParam, processContributors }

object GithubRepositoryImpl {
  def implementation[F[_]: Sync](client: Client[F],
                                 tokenAuthorization: Authorization): GithubRepository[F] =
    new GithubRepository[F] with GithubAuthorization {
      override implicit val authorization: Authorization = tokenAuthorization
      val dsl: Http4sClientDsl[F]                        = new Http4sClientDsl[F] {}
      import dsl._
      def buildGithubRepositoryUrl(orgName: String): Uri =
        GithubBaseUrl / OrgTag / orgName / RepoTag
      def executeRequest[T](uri: Uri)(
        implicit d: EntityDecoder[F, T],
        authorization: Authorization
      ): F[T] =
        client.expect[T](GET(uri, authorization))

      def getGithubRepos(name: String, params: Map[String, Int]): F[List[Repository]] =
        executeRequest[List[Repository]](buildGithubRepositoryUrl(name).withQueryParams(params))

      def getGithubContributionsPerRepo(contributionsUri: Uri,
                                        params: Map[String, Int]): F[List[Contribution]] =
        executeRequest[List[Contribution]](contributionsUri.withQueryParams(params))

      override def provide(name: String,
                           limitRepo: Option[Int],
                           limitContributors: Option[Int]): F[List[GithubResponse]] =
        (for {
          repos <- getGithubRepos(name, buildPaginationParam(limitRepo))
          contributions <- repos
            .map(
              repo =>
                getGithubContributionsPerRepo(Uri.unsafeFromString(repo.`contributors_url`),
                                              buildPaginationParam(limitContributors))
            )
            .sequence
          cleanedContributions = processContributors(contributions.flatten)
        } yield cleanedContributions).adaptError {
          case t => GithubError(t.getMessage)
        }
    }
}
