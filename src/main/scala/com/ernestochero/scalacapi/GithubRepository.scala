package com.ernestochero.scalacapi

import cats.Applicative
import cats.effect.Sync
import com.ernestochero.scalacapi.GithubRepository.GithubResponse
import io.circe.generic.semiauto.{ deriveDecoder, deriveEncoder }
import io.circe.{ Decoder, Encoder }
import org.http4s.circe.{ jsonEncoderOf, jsonOf }
import org.http4s.headers.Authorization
import org.http4s.{ EntityDecoder, EntityEncoder }
trait GithubRepository[F[_]] {
  def provide(name: String,
              limitRepo: Option[Int],
              limitContributors: Option[Int]): F[List[GithubResponse]]
}
trait GithubAuthorization {
  implicit val authorization: Authorization
}
object GithubRepository {
  def apply[F[_]](implicit ev: GithubRepository[F]): GithubRepository[F] = ev
  final case class GithubResponse(name: String, contributions: Long)
  object GithubResponse {
    implicit val githubDecoder: Decoder[GithubResponse]                                  = deriveDecoder[GithubResponse]
    implicit def githubEntityDecoder[F[_]: Sync]: EntityDecoder[F, List[GithubResponse]] = jsonOf
    implicit val githubEncoder: Encoder[GithubResponse]                                  = deriveEncoder[GithubResponse]
    implicit def githubEntityEncoder[F[_]: Applicative]: EntityEncoder[F, List[GithubResponse]] =
      jsonEncoderOf
  }

  final case class Repository(id: Long, name: String, `contributors_url`: String)
  object Repository {
    implicit val repositoryDecoder: Decoder[Repository] =
      deriveDecoder[Repository]
    implicit def repositoryEntityDecoder[F[_]: Sync]: EntityDecoder[F, List[Repository]] =
      jsonOf
    implicit val repositoryEncoder: Encoder[Repository] =
      deriveEncoder[Repository]
    implicit def repositoryEntityEncoder[F[_]: Applicative]: EntityEncoder[F, List[Repository]] =
      jsonEncoderOf
  }

  final case class Contribution(login: String, contributions: Long)
  object Contribution {
    implicit val contributionDecoder: Decoder[Contribution] =
      deriveDecoder[Contribution]
    implicit def contributionEntityDecoder[F[_]: Sync]: EntityDecoder[F, List[Contribution]] =
      jsonOf
    implicit val contributionEncoder: Encoder[Contribution] =
      deriveEncoder[Contribution]
    implicit def contributionEntityEncoder[F[_]: Applicative]
      : EntityEncoder[F, List[Contribution]] =
      jsonEncoderOf
  }
  case class GithubError(message: String) extends Exception
}
