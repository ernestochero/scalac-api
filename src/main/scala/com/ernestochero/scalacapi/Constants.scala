package com.ernestochero.scalacapi

import org.http4s.implicits.http4sLiteralsSyntax

object Constants {
  val GithubBaseUrl        = uri"https://api.github.com"
  val OrgTag               = "orgs"
  val RepoTag              = "repos"
  val LimitRepoTag         = "limit_repo"
  val LimitContributorsTag = "limit_contributors"
  val PerPageTag           = "per_page"
}
