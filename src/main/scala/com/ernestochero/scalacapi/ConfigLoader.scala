package com.ernestochero.scalacapi

import cats.effect.{ Blocker, ContextShift, Sync }
import pureconfig.ConfigSource
import pureconfig._
import pureconfig.generic.auto._
import pureconfig.module.catseffect.syntax._

case class HttpConf(host: String, port: Int)
case class AppConfig(httpConf: HttpConf, token: String)
object ConfigLoader {
  def readConfig[F[_]: Sync: ContextShift](blocker: Blocker): F[AppConfig] =
    ConfigSource.default.loadF[F, AppConfig](blocker)
}
