package com.ernestochero.scalacapi

import cats.effect.Sync
import org.http4s.HttpRoutes
import org.http4s.dsl.Http4sDsl
import cats.implicits._
import com.ernestochero.scalacapi.Constants.{ LimitContributorsTag, LimitRepoTag }
import org.http4s.dsl.impl.OptionalQueryParamDecoderMatcher

object LimitRepoQueryParamMatcher extends OptionalQueryParamDecoderMatcher[Int](LimitRepoTag)
object LimitContributorsQueryParamMatcher
    extends OptionalQueryParamDecoderMatcher[Int](LimitContributorsTag)
object ScalacApiRoutes {
  def githubRoutes[F[_]: Sync](
    repository: GithubRepository[F]
  )(implicit H: HttpErrorHandler[F, Throwable]): HttpRoutes[F] = {
    val dsl = new Http4sDsl[F] {}
    import dsl._
    val routes = HttpRoutes.of[F] {
      case GET -> Root / "org" / name / "contributors" :? LimitRepoQueryParamMatcher(maybeLimitRepo) +& LimitContributorsQueryParamMatcher(
            maybeLimitContributors
          ) =>
        repository
          .provide(name, maybeLimitRepo, maybeLimitContributors)
          .flatMap(Ok(_))
    }
    H.handle(routes)
  }
}
