package com.ernestochero.scalacapi

import com.ernestochero.scalacapi.Constants.PerPageTag
import com.ernestochero.scalacapi.GithubHelper.{ buildPaginationParam, processContributors }
import com.ernestochero.scalacapi.GithubRepository.{ Contribution, GithubResponse }
import org.specs2.matcher.MatchResult

class GithubHelperSpec extends org.specs2.mutable.Specification {

  "processContributors function" >> {
    "return sorted list of repository response" >> {
      processContributorsTest()
    }
  }

  "buildPaginationParams function" >> {
    "return params in a Map structure built from an Optional Parameter" >> {
      val paramsResult = buildPaginationParam(
        Option(10)
      )
      paramsResult(PerPageTag) must_== 10
    }
    "return an Empty Map structure built from an Optional Parameter" >> {
      val paramsResult = buildPaginationParam(
        Option.empty
      )
      paramsResult.get(PerPageTag) must_== Option.empty
      paramsResult.isEmpty must_== true
    }
  }

  def checkCleanContributionFunction(actualResult: List[GithubResponse],
                                     expectedResult: List[GithubResponse]): Boolean = {
    val lengthCheck = actualResult.length === expectedResult.length
    val maxCheck    = actualResult.head === expectedResult.head
    val minCheck    = actualResult.last === expectedResult.last
    lengthCheck && maxCheck && minCheck
  }

  def processContributorsTest(): MatchResult[Boolean] = {
    val fakeContributionList: List[Contribution] = List(
      Contribution("a", 3),
      Contribution("b", 3),
      Contribution("c", 2),
      Contribution("b", 6),
      Contribution("a", 1)
    )
    val expectedGithubResponseList: List[GithubResponse] = List(
      GithubResponse("b", 9),
      GithubResponse("a", 4),
      GithubResponse("c", 2),
    )

    checkCleanContributionFunction(
      processContributors(fakeContributionList),
      expectedGithubResponseList
    ) must_=== true
  }
}
