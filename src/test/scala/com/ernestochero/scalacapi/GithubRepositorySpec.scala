package com.ernestochero.scalacapi

import cats.effect.IO
import com.ernestochero.scalacapi.GithubRepository.GithubResponse
import io.circe.Json
import org.http4s.circe._
import org.http4s.implicits._
import org.http4s.implicits.http4sLiteralsSyntax
import org.http4s.{ EntityDecoder, Method, Request, Response, Status }
import org.specs2.matcher.MatchResult

class GithubRepositorySpec extends org.specs2.mutable.Specification with org.specs2.mock.Mockito {

  def check[A](actual: IO[Response[IO]], expectedStatus: Status, expectedBody: Option[A])(
    implicit ev: EntityDecoder[IO, A]
  ): Boolean = {
    val actualResp  = actual.unsafeRunSync
    val statusCheck = actualResp.status == expectedStatus
    val bodyCheck = expectedBody.fold[Boolean](
      actualResp.body.compile.toVector.unsafeRunSync.isEmpty
    )(
      expected => actualResp.as[A].unsafeRunSync == expected
    )
    statusCheck && bodyCheck
  }

  def githubAPIReturnValidCheck(): MatchResult[Boolean] = {
    implicit val githubHttpErrorHandler: HttpErrorHandler[IO, Throwable] =
      new GithubRepositoryErrorHandler[IO]

    val successRequest: GithubRepository[IO] =
      (_: String, _: Option[Int], _: Option[Int]) =>
        IO.pure(List(GithubResponse("rossabaker", 4573), GithubResponse("jonsnow", 1063)))

    val expectedJson: Json = Json.arr(
      Json.obj(
        ("name", Json.fromString("rossabaker")),
        ("contributions", Json.fromBigInt(4573))
      ),
      Json.obj(
        ("name", Json.fromString("jonsnow")),
        ("contributions", Json.fromBigInt(1063))
      )
    )
    val response = ScalacApiRoutes
      .githubRoutes[IO](successRequest)
      .orNotFound
      .run(
        Request(method = Method.GET, uri = uri"/org/fake/contributors")
      )
    check[Json](response, Status.Ok, Some(expectedJson)) must_=== true
  }

  "Github Repository API" >> {
    "return valid check" >> {
      githubAPIReturnValidCheck()
    }
  }
}
